defmodule HoundPlaygroundBasic do
  @moduledoc """
  Documentation for HoundPlaygroundBasic.
  """

  @doc """
  Hello world.

  ## Examples

      iex> HoundPlaygroundBasic.hello
      :world

  """
  
  # $ ~/tools/phantomjs-2.1.1-linux-x86_64/bin/phantomjs --wd
  # iex> HoundPlaygroundBasic.fetch_ip
  use Hound.Helpers

  def fetch_ip do
    # starts Hound session. Required before we can do anything
    Hound.start_session

    # visit the website which shows the visitor's IP
    navigate_to "http://icanhazip.com"  # defined in Hound.Helpers.Navigation module

    # display its raw source
    IO.inspect page_source()  # defined in Hound.Helpers.Page module

    # cleanup
    Hound.end_session
  end

end
