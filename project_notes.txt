Hound Tutorial
--------------
https://semaphoreci.com/community/tutorials/end-to-end-testing-in-elixir-with-hound

https://hexdocs.pm/hound/readme.html

https://www.quora.com/How-does-the-Selenium-WebDriver-work


Project: hound_playground_basic
-------------------------------
1: install PhantomJS
	http://phantomjs.org/download
	extract to ~/tools

2: create basic hound playground:
	$ mix new hound_playground_basic
    $ cd hound_playground_basic

	- mix.exs: Add hound to deps and application blocks
	- config/config.exs: Configure hound to use GhostDriver version of WebDriver from PhantomJS
	- lib/hound_playground_basic: Implement a hound call to http://icanhazip.com to return our IP address

3: get project dependencies
	$ mix deps.get
	$ mix deps.compile

4: start PhantomJS in WebDriver mode:
	$ ~/tools/phantomjs-2.1.1-linux-x86_64/bin/phantomjs --wd

5: start hound_playground in iex and run the hound function:
	$ iex -S mix
	> HoundPlaygroundBasic.fetch_ip
